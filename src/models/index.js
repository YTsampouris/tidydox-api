import mongoose from 'mongoose';

import User from './user';
import Message from './message';
import Document from './document';
import Contact from './contact';
import Department from './department';
import File from './file';
import Tag from './tag';

const connectDb = () => {
  if (process.env.TEST_DATABASE_URL) {
    return mongoose.connect(process.env.TEST_DATABASE_URL, {
      useNewUrlParser: true,
    });
  }

  if (process.env.DATABASE_URL) {
    return mongoose.connect(process.env.DATABASE_URL, {
      useNewUrlParser: true,
    });
  }
};

const models = { User, Message, Document, Contact, Department, File, Tag };

export { connectDb };

export default models;
