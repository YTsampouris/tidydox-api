import { combineResolvers } from 'graphql-resolvers';
import { AuthenticationError, UserInputError } from 'apollo-server';
import Mongoose from 'mongoose';
import { isAuthenticated, isAdmin } from './authorization';

export default {
  Query: {
    tags: async (parent, args, { models }) => {
      return await models.Tag.find();
    },
  },

  Mutation: {
    createTag: combineResolvers(
      isAuthenticated, isAdmin,
      async (
        parent,
        {
          text
        },
        { models },
      ) => {
        const id = new Mongoose.Types.ObjectId();
        const tag = await models.Tag.create({
          _id: id,
          text
        });
        // console.log('created tag:: ', tag);
        return tag;
      },
    )
  }
};
