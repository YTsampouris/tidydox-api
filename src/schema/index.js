import { gql } from 'apollo-server-express';

import userSchema from './user';
import messageSchema from './message';
import documentSchema from './document';
import contactSchema from './contact';
import departmentSchema from './department';
import fileSchema from './file';
import tagSchema from './tag';

const linkSchema = gql`
  scalar Date

  type Query {
    _: Boolean
  }

  type Mutation {
    _: Boolean
  }

  type Subscription {
    _: Boolean
  }
`;

export default [
  linkSchema,
  userSchema,
  messageSchema,
  documentSchema,
  contactSchema,
  departmentSchema,
  fileSchema,
  tagSchema
];
