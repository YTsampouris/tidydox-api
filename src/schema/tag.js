import { gql } from 'apollo-server-express';

export default gql`
  extend type Query {
    tags: [Tag]
  }

  extend type Mutation {
    createTag(
      text: String
    ): Tag!
  }

  type Tag {
    _id: ID!
    text: String
  }
`;
