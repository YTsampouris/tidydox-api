import mongoose from 'mongoose';

const tagSchema = new mongoose.Schema({
  _id: {
    type: mongoose.Schema.Types.ObjectId,
    required: false,
  },
  text: {
    type: String,
    required: false,
  }
});

const Tag = mongoose.model('Tag', tagSchema);

export default Tag;
