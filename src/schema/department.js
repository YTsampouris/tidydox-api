import { gql } from 'apollo-server-express';

export default gql`
  extend type Query {
    departments: [Department!]
    department(id: ID!): Department
  }

  extend type Mutation {
    createDepartment(title: String, description: String): Department!
    updateDepartmentManager(_id: ID, manager: ID): Department!
    updateDepartment(
      _id: ID
      title: String
      description: String
      manager: ID
    ): Department!
    deleteDepartment(_id: ID!): Boolean!
  }

  type Department {
    _id: ID
    title: String
    description: String
    manager: ID
  }
`;
