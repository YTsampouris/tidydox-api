import mongoose from 'mongoose';

const documentSchema = new mongoose.Schema({
  title: {
    type: String,
    required: false,
  },
  description: {
    type: String,
    required: false,
  },
  sender: {
    type: String,
    required: false,
  },
  receivers: {
    type: Array,
    required: false,
  },
  date: {
    type: Date,
    required: false,
  },
  files: {
    type: Array,
    required: false,
  },
  transactionType: {
    type: String,
    required: false,
  },
  timestamp: {
    type: Date,
    default: Date.now,
    required: false,
  },
  protocolNumber: {
    type: Number
  },
  tags: {
    type: String,
    required: false,
  },
  // TODO use the ref: User in production
  // userId: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  // userId: { type: String, required: true },
});

const Document = mongoose.model('Document', documentSchema);

export default Document;
