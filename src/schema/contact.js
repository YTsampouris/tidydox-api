import { gql } from 'apollo-server-express';

export default gql`
  extend type Query {
    contacts: [Contact!]
    contact(id: ID!): Contact
  }

  extend type Mutation {
    createContact(
      name: String
      middlename: String
      surname: String
      organization: String
      street: String
      streetNumber: String
      postalCode: String
      area: String
      city: String
      website: String
      email: String
      position: String
      department: String
      phone: String
      fax: String
      country: String
      region: String
      prefecture: String
    ): Contact!
    updateContact(
      _id: ID
      name: String
      middlename: String
      surname: String
      organization: String
      street: String
      streetNumber: String
      postalCode: String
      area: String
      city: String
      website: String
      email: String
      position: String
      department: String
      phone: String
      fax: String
      country: String
      region: String
      prefecture: String
    ): Contact!
    deleteContact(_id: ID!): Boolean!
  }

  type Contact {
    _id: ID
    name: String
    middlename: String
    surname: String
    organization: String
    street: String
    streetNumber: String
    postalCode: String
    area: String
    city: String
    website: String
    email: String
    position: String
    department: String
    phone: String
    fax: String
    country: String
    region: String
    prefecture: String
  }
`;
