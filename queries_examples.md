# Write your query or mutation here

query getUsers {
users {
username
}
}

# Write your query or mutation here

query getAllDocuments {
documents{
\_id
title
userId
}
}

# Write your query or mutation here

query getDocument {
document(\_id: "60032a6b083ac45a68e4cc7f") {
\_id
title
userId
}
}

mutation {
createDocument (title: "creation test3", userId: "just me") {
title
userId
}
}

mutation {
deleteDocument (\_id: "60032b3dcf8a9c22b404e19b")
}

mutation {
editDocument(\_id: "60032a6b083ac45a68e4cc7f", title: "changed title") {
\_id
title
userId
}
}
