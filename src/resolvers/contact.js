import { combineResolvers } from 'graphql-resolvers';
import { AuthenticationError, UserInputError } from 'apollo-server';
import Mongoose from 'mongoose';

export default {
  Query: {
    contacts: async (parent, args, { models }) => {
      return await models.Contact.find();
    },
    contact: async (parent, { id }, { models }) => {
      return await models.Contact.findById(id);
    },
  },

  Mutation: {
    createContact: combineResolvers(
      // isAuthenticated,
      async (
        parent,
        {
          name,
          middlename,
          surname,
          organization,
          street,
          streetNumber,
          postalCode,
          area,
          city,
          website,
          email,
          position,
          department,
          phone,
          fax,
          country,
          region,
          prefecture,
        },
        { models },
      ) => {
        const id = new Mongoose.Types.ObjectId();
        const contact = await models.Contact.create({
          _id: id,
          name,
          middlename,
          surname,
          organization,
          street,
          streetNumber,
          postalCode,
          area,
          city,
          website,
          email,
          position,
          department,
          phone,
          fax,
          country,
          region,
          prefecture,
        });
        return contact;
      },
    ),

    updateContact: combineResolvers(
      // isAuthenticated,
      async (
        parent,
        {
          _id,
          name,
          middlename,
          surname,
          organization,
          street,
          streetNumber,
          postalCode,
          area,
          city,
          website,
          email,
          position,
          department,
          phone,
          fax,
          country,
          region,
          prefecture,
        },
        { models },
      ) => {
        console.log(
          'update contact:: ',
          _id,
          name,
          middlename,
          surname,
          organization,
          street,
          streetNumber,
          postalCode,
          area,
          city,
          website,
          email,
          position,
          department,
          phone,
          fax,
          country,
          region,
          prefecture,
        );
        return await models.Contact.findByIdAndUpdate(
          _id,
          {
            $set: {
              name,
              middlename,
              surname,
              organization,
              street,
              streetNumber,
              postalCode,
              area,
              city,
              website,
              email,
              position,
              department,
              phone,
              fax,
              country,
              region,
              prefecture,
            },
          },
          { new: true },
        );
      },
    ),

    deleteContact: combineResolvers(
      // isAdmin,
      async (parent, { _id }, { models }) => {
        const contact = await models.Contact.findById(_id);
        if (contact) {
          await contact.remove();
          return true;
        } else {
          return false;
        }
      },
    ),
  },
};
