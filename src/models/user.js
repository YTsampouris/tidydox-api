import mongoose from 'mongoose';

import bcrypt from 'bcrypt';
import isEmail from 'validator/lib/isEmail';

const userSchema = new mongoose.Schema({  
  _id: {
    type: mongoose.Schema.Types.ObjectId,
    required: false,
  },
  username: {
    type: String,
    unique: true,
    required: true,
  },
  fullname: {
    type: String,
    unique: false,
    required: false,
  },
  department: [
    { type: mongoose.Schema.Types.ObjectId, ref: 'Department' },
  ],
  email: {
    type: String,
    unique: true,
    required: false,
    // TODO enable the validator
    // validate: [isEmail, 'No valid email address provided.'],
  },
  password: {
    type: String,
    required: false,
    minlength: 7,
    maxlength: 42,
  },
  role: {
    type: String,
    required: false,
  },
});

userSchema.statics.findByLogin = async function (login) {
  let user = await this.findOne({
    username: login,
  });

  if (!user) {
    user = await this.findOne({ email: login });
  }

  return user;
};

// userSchema.pre('remove', function (next) {
//   this.model('Message').deleteMany({ userId: this._id }, next);
// });

userSchema.pre('save', async function () {
  this.password = await this.generatePasswordHash();
});

userSchema.methods.generatePasswordHash = async function () {
  const saltRounds = 10;
  return await bcrypt.hash(this.password, saltRounds);
};

userSchema.methods.validatePassword = async function (password) {
  return await bcrypt.compare(password, this.password);
};

const User = mongoose.model('User', userSchema);

export default User;
