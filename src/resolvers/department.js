import { combineResolvers } from 'graphql-resolvers';
import { AuthenticationError, UserInputError } from 'apollo-server';
import Mongoose from 'mongoose';

export default {
  Query: {
    departments: async (parent, args, { models }) => {
      return await models.Department.find();
    },
    department: async (parent, { id }, { models }) => {
      return await models.Department.findById(id);
    },
  },

  Mutation: {
    createDepartment: combineResolvers(
      // isAuthenticated,
      async (parent, { title, description }, { models }) => {
        const id = new Mongoose.Types.ObjectId();
        const department = await models.Department.create({
          _id: id,
          title,
          description,
        });
        return department;
      },
    ),

    updateDepartment: combineResolvers(
      // isAuthenticated,
      async (
        parent,
        { _id, title, description, manager },
        { models },
      ) => {
        return await models.Department.findByIdAndUpdate(
          _id,
          {
            $set: {
              title,
              description,
              manager,
            },
          },
          { new: true },
        );
      },
    ),

    updateDepartmentManager: combineResolvers(
      // isAuthenticated,
      async (parent, { _id, manager }, { models }) => {
        console.log(_id, manager);
        return await models.Department.findByIdAndUpdate(
          _id,
          {
            $set: {
              manager,
            },
          },
          { new: true },
        );
      },
    ),

    deleteDepartment: combineResolvers(
      // isAdmin,
      async (parent, { _id }, { models }) => {
        const department = await models.Department.findById(_id);
        if (department) {
          await department.remove();
          return true;
        } else {
          return false;
        }
      },
    ),
  },
};
