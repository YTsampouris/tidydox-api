import mongoose from 'mongoose';

const contactSchema = new mongoose.Schema({
  _id: {
    type: mongoose.Schema.Types.ObjectId,
    required: false,
  },
  name: {
    type: String,
    required: false,
  },
  middlename: {
    type: String,
    required: false,
  },
  surname: {
    type: String,
    required: false,
  },
  organization: {
    type: String,
    required: false,
  },
  street: {
    type: String,
    required: false,
  },
  streetNumber: {
    type: String,
    required: false,
  },
  postalCode: {
    type: String,
    required: false,
  },
  area: {
    type: String,
    required: false,
  },
  city: {
    type: String,
    required: false,
  },
  website: {
    type: String,
    required: false,
  },
  email: {
    type: String,
    required: false,
  },
  position: {
    type: String,
    required: false,
  },
  department: {
    type: String,
    required: false,
  },
  phone: {
    type: String,
    required: false,
  },
  fax: {
    type: String,
    required: false,
  },
  country: {
    type: String,
    required: false,
  },
  region: {
    type: String,
    required: false,
  },
  prefecture: {
    type: String,
    required: false,
  },
});

const Contact = mongoose.model('Contact', contactSchema);

export default Contact;
