import { gql } from 'apollo-server-express';

export default gql`
  extend type Query {
    documents(page: Int): [Document]!
    document(_id: ID): requestedDocument
    searchDocuments(searchText: String): [Document]
    searchDocumentsByDate(searchDateRangeFrom: String, searchDateRangeTo: String): [Document]
  }
  extend type Mutation {
    createDocument(
      title: String!
      description: String
      sender: ID
      receivers: [ID]
      date: Date
      files: [ID]
      transactionType: String
      timestamp: String
      tags: String
    ): Document!
    deleteDocument(_id: ID!): Boolean!
    editDocument(_id: ID!, title: String!): Document!
  }
  type Document {
    _id: ID!
    title: String
    description: String
    sender: ID!
    receivers: [ID!]
    date: Date
    protocolNumber: Int
    files: [ID]
    transactionType: String
    timestamp: String
    tags: String
  }
  type requestedDocument {
    _id: ID!
    title: String
    description: String
    date: Date
    transactionType: String
    timestamp: Date
    protocolNumber: Int
    files: String
    sender: String
    receivers: String
    tags: String
  }
  type requestedFile {
    _id: ID
    length: Int
    chunkSize: Int
    uploadDate: Date
    filename: String
    md5: String
    contentType: String
  }`;
