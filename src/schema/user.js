import { gql } from 'apollo-server-express';

export default gql`
  extend type Query {
    users: [User!]
    usersByDepartment(_id: ID): [User]
    user(id: ID!): User
    me: User
  }

  extend type Mutation {
    signUp(
      username: String!
      email: String!
      password: String!
    ): Token!

    signIn(email: String!, password: String!): Token!
    createUser(
      _id: String
      username: String
      fullname: String
      email: String
      password: String
      department: [ID]
    ): User!
    updateUser(
      _id: ID!
      username: String
      fullname: String
      password: String
      email: String
      department: [ID]
    ): User!
    deleteUser(_id: ID!): Boolean!
  }

  type Token {
    token: String!
    user_id: String!
    username: String!
    email: String!
    role: String
  }

  type User {
    _id: ID
    username: String
    fullname: String
    password: String
    messages: [Message!]
    department: [Department]
    email: String
    role: String
  }
`;
