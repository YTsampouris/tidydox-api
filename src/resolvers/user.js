import jwt from 'jsonwebtoken';
import { combineResolvers } from 'graphql-resolvers';
import { AuthenticationError, UserInputError } from 'apollo-server';
import Mongoose from 'mongoose';

import { isAdmin, isAuthenticated } from './authorization';

const createToken = async (user, secret, expiresIn) => {
  const { id, email, username, role } = user;
  return await jwt.sign({ id, email, username, role }, secret, {
    expiresIn,
  });
};

export default {
  Query: {
    users: async (parent, args, { models }) => {
      return await models.User.find({}).populate({
        path: 'department',
        model: 'Department',
      });
    },
    user: async (parent, { id }, { models }) => {
      return await models.User.findById(id);
    },
    usersByDepartment: async (parent, { _id }, { models }) => {
      console.log('getting users by department #################');
      const users = await models.User.find({
        department: _id,
      }).populate({
        path: 'department',
        model: 'Department',
      });
      console.log('users by department:: ', users);
      return users;
    },
    me: async (parent, args, { models, me }) => {
      if (!me) {
        return null;
      }

      return await models.User.findById(me.id);
    },
  },

  Mutation: {
    signUp: async (
      parent,
      { username, email, password },
      { models, secret },
    ) => {
      const user = await models.User.create({
        username,
        email,
        password,
      });

      return { token: createToken(user, secret, '480m') };
    },

    signIn: async (
      parent,
      { email, password },
      { models, secret },
    ) => {
      const user = await models.User.findByLogin(email);

      if (!user) {
        throw new UserInputError(
          'No user found with this login credentials.',
        );
      }

      const isValid = await user.validatePassword(password);

      if (!isValid) {
        throw new AuthenticationError('Invalid password.');
      }
      // console.log(user._id);
      return {
        token: createToken(user, secret, '480m'),
        user_id: user._id,
        username: user.username,
        email: user.email,
        role: user.role,
      };
    },

    createUser: combineResolvers(
      // isAuthenticated,
      async (
        parent,
        { username, fullname, password, department, email },
        { models },
      ) => {
        const id = new Mongoose.Types.ObjectId();
        const user = await models.User.create({
          _id: id,
          username,
          fullname,
          password,
          department,
          email,
          role: 'user'
        });
        return user;
      },
    ),

    updateUser: combineResolvers(
      // isAuthenticated,
      async (
        parent,
        { _id, username, fullname, password, department, email },
        { models },
      ) => {
        return await models.User.findByIdAndUpdate(
          _id,
          {
            $set: {
              username,
              fullname,
              password,
              department,
              email,
            },
          },
          { new: true },
        );
      },
    ),

    deleteUser: combineResolvers(
      // isAdmin,
      async (parent, { _id }, { models }) => {
        const user = await models.User.findById(_id);

        if (user) {
          await user.remove();
          return true;
        } else {
          return false;
        }
      },
    ),
  },

  User: {
    messages: async (user, args, { models }) => {
      return await models.Message.find({
        userId: user.id,
      });
    },
  },
};
