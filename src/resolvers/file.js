import mongodb from 'mongodb';
const mongoose = require('mongoose');
import { combineResolvers } from 'graphql-resolvers';
import { isAuthenticated, isMessageOwner } from './authorization';

const storeFile = async (upload) => {
  const { filename, createReadStream, mimetype } = await upload.then(
    (result) => result,
  );

  const bucket = new mongoose.mongo.GridFSBucket(
    mongoose.connection.db,
    { bucketName: 'documents' },
  );

  const uploadStream = bucket.openUploadStream(filename, {
    contentType: mimetype,
  });
  return new Promise((resolve, reject) => {
    createReadStream()
      .pipe(uploadStream)
      .on('error', (err) => {
        console.log('Error Event Emitted', err);
        reject({
          success: false,
          message: 'Failed',
        });
      })
      .on('finish', () => {
        resolve({
          success: true,
          message: 'Successfully Uploaded',
          _id: uploadStream.id,
          mimetype,
          filename,
        });
      });
  });
};

const downloadFile = async (_id) => {
  const bucket = new mongoose.mongo.GridFSBucket(mongoose.connection.db, {
    bucketName: 'documents',
  });
  return new Promise((resolve, reject) => {
    // temporary variable to hold image
    var data = [];

    // create the download stream
    const readstream = bucket.openDownloadStream(mongoose.Types.ObjectId(_id));
    readstream.on('data', function (chunk) {
      data.push(chunk);
    });
    readstream.on('error', async (error) => {
      reject(error);
    });
    readstream.on('end', async () => {
      let bufferBase64 = Buffer.concat(data);
      const img = bufferBase64.toString('base64');
      resolve(img);
    });
  });
};

export default {
  Query: {
    uploadedFiles: async (parent, { _id }, { models }) => {
      return await models.File.findById(_id);
    },
    file: async (parent, { _id }, { models }) => {
      // console.log('getting file:..............', _id);
      const Schema = mongoose.Schema;
      var gridSchema = new Schema({},{ strict: false });
      var Grid = mongoose.model("Grid", gridSchema, "documents.files" );
      Grid.findOne({_id: _id},function(err,gridfiles) {
        if (err) throw err;
        console.log( gridfiles );
      });
      return;
    },
    downloadFile: async (parent, { _id }, { models }) => {
      try {
        const img = await downloadFile(_id);
        // console.log(img);
        return img;
      } catch (error) {
        throw new Error(error);
      }
    }
  },

  Mutation: {
    singleUploadLocal: combineResolvers(
      // isAuthenticated,
      async (parent, { doc_id, file }, { models }) => {
        const fileid = await storeFile(file).then((result) => {
          return models.Document.findByIdAndUpdate(
            doc_id,
            { $push: { files: result._id } },
          );
        });
        return fileid;
      },
    ),
    deleteFile: combineResolvers(async (parent, _id, { models }) => {
      // console.log('deleting files');
      const file = await models.File.findById(_id);

      if (file) {
        await file.remove();
        return true;
      } else {
        return false;
      }
    }),
  },
};
