import mongoose from 'mongoose';

const fileSchema = new mongoose.Schema({
  _id: {
    type: mongoose.Schema.Types.ObjectId,
    required: false,
  },
  filename: {
    type: String,
    required: false,
  },
  mimetype: {
    type: String,
    required: false,
  },
  encoding: {
    type: String,
    required: false,
  },
});

const File = mongoose.model('File', fileSchema);

export default File;
