import { combineResolvers } from 'graphql-resolvers';
import mongoose from 'mongoose';
import pubsub, { EVENTS } from '../subscription';
import { isAuthenticated, isAdmin } from './authorization';

const Schema = mongoose.Schema;
const gridSchema = new Schema({},{ strict: false });
const Grid = mongoose.model("Grid", gridSchema, "documents.files" );

const toCursorHash = (string) =>
  Buffer.from(string).toString('base64');

const fromCursorHash = (string) =>
  Buffer.from(string, 'base64').toString('ascii');

export default {
  Query: {
    searchDocuments: combineResolvers(
      isAuthenticated,
      // isAdmin,
      async (
        parent,
        { searchText },
        { models, me },
      ) => {
        let documents = null;
        // if (me.role === 'admin') {
        documents = await models.Document.find({
          $or: [
            {
              title: {
                $regex: new RegExp(searchText),
                $options: 'i',
              },
            },
            {
              description: {
                $regex: new RegExp(searchText),
                $options: 'i',
              },
            },
            {
              tags: {
                $regex: new RegExp(searchText),
                $options: 'i',
              },
            },
          ],
        });
        // } else {
        //   documents = await models.Document.find({
        //     $or: [{ receivers: me.id }, { sender: me.id }],
        //   });
        // }
        return documents;
      },
    ),
    searchDocumentsByDate: combineResolvers(
      isAuthenticated,
      // isAdmin,
      async (
        parent,
        { searchDateRangeFrom, searchDateRangeTo },
        { models, me },
      ) => {
        let documents = null;
        documents = await models.Document.find(
          { date: {
              $gte: searchDateRangeFrom,
              $lte: searchDateRangeTo
            }
          }
        );
        return documents;
      },
    ),
    documents: combineResolvers(
      isAuthenticated,
      // isAdmin,
      async (parent, { page }, { models, me }) => {
        // const cursorOptions = cursor
        //   ? {
        //       createdAt: {
        //         $lt: fromCursorHash(cursor),
        //       },
        //     }
        //   : {};
        // const documents = await models.Document.find(
        //   cursorOptions,
        //   { $or: [{ receivers: me.id }, { sender: me.id }] },
        //   {
        //     sort: { timestamp: -1 },
        //     limit: limit + 1,
        //   },
        // );
        let documents = null;
        const limit = 50;
        if (me.role === 'admin') {
          documents = await models.Document.find().skip(page*limit).limit(limit);
          // console.log(documents, me.role)
        }
        if (me.role === 'user') {
          const myDepartment = await models.Department.find({manager: me.id});
          if (myDepartment) {
            const myDepartmentId = myDepartment[0]._id;
            const usersOfMyDepartment = await models.User.find({ department: myDepartmentId })
            let usersOfMyDepartmentIds = [];
            usersOfMyDepartment.forEach((user) => {
              if (user.role !== 'admin') {
                usersOfMyDepartmentIds.push(user._id);
              }
            });
            // console.log('me:: ', me.id, myDepartmentId, usersOfMyDepartmentIds);
            documents = await models.Document.find({
              $or: [{ receivers: { $in : usersOfMyDepartmentIds } }, { sender: { $in : usersOfMyDepartmentIds } }],
            });
          }
        }

        // const hasNextPage = documents.length > limit;
        // const edges = hasNextPage ? documents.slice(0, -1) : documents;
        // console.log(documents);
        return documents;
        // edges,
        // pageInfo: {
        //   hasNextPage,
        //   endCursor: toCursorHash(
        //     edges[edges.length - 1].createdAt.toString(),
        //   ),
        // },
      },
    ),
    document: async (parent, { _id }, { models }) => {
      const requestedDocument = {};
      const document = await models.Document.findById(_id);
      const fileIds = [];
      document.files.forEach((f) => {
        fileIds.push(mongoose.Types.ObjectId(f));
      });
      // console.log('file ids:: ', fileIds, fileIds[0], typeof(fileIds[0]));
      const files = await Grid.find({_id: { $in: fileIds }},function(err,gridfiles) {
        if (err) throw err;
        // console.log( gridfiles );
        return gridfiles;
      });

      const sender = await models.User.find({ _id : document.sender}, { department: 1, _id: 1, username:1, fullname:1, email: 1});
      const receivers = await models.User.find({ _id : document.receivers[0]});
      const contactsReceived = await models.Contact.find({ _id : document.receivers[0]});
      const contactsSent = await models.Contact.find({ _id : document.sender});
      // console.log("receivers are:", receivers, contactsReceived, document.receivers);
      // console.log('files found:: ', files);

      requestedDocument._id = document._id;
      requestedDocument.title = document.title;
      requestedDocument.description = document.description;
      requestedDocument.date = document.date;
      requestedDocument.transactionType = document.transactionType;
      requestedDocument.timestamp = document.timestamp;
      requestedDocument.protocolNumber = document.protocolNumber;
      requestedDocument.files = JSON.stringify(files);
      requestedDocument.sender = JSON.stringify(sender.concat(contactsSent));
      requestedDocument.receivers = JSON.stringify(receivers.concat(contactsReceived));
      requestedDocument.tags = document.tags;

      // console.log('the document is:: ', requestedDocument);
      return requestedDocument;
    },
  },

  Mutation: {
    createDocument: combineResolvers(
      // isAuthenticated,
      async (
        parent,
        {
          title,
          description,
          sender,
          receivers,
          date,
          files,
          transactionType,
          timestamp,
          tags
        },
        { models },
      ) => {
        let nextProtocolNumber = 0;
        const mostRecentDocument = await models.Document.find({}).sort({"date" : -1}).limit(1);
        if (mostRecentDocument.length === 0) {
          nextProtocolNumber = 1;
        } else {
          if (mostRecentDocument[0].date.getYear() < date.getYear()) {
            nextProtocolNumber = 1;
          } else {
            nextProtocolNumber = parseInt(mostRecentDocument[0].protocolNumber) + 1;
          }
        }
        const document = await models.Document.create({
          title,
          description,
          sender,
          receivers,
          date,
          files,
          transactionType,
          timestamp,
          protocolNumber: nextProtocolNumber,
          tags
        });

        // pubsub.publish(EVENTS.DOCUMENT.CREATED, {
        //   documentCreated: { document },
        // });
        // console.log('document is ::', document);
        return document;
      },
    ),
    deleteDocument: combineResolvers(
      async (parent, { _id }, { models }) => {
        const document = await models.Document.findById(_id);
        // console.log(document.files);
        // console.log(document.files[0]);
        const bucket = new mongoose.mongo.GridFSBucket(
          mongoose.connection.db,
          { bucketName: 'documents' },
        );
        document.files.forEach((file) => {
          // TODO https://stackoverflow.com/questions/27482806/check-if-id-exists-in-a-collection-with-mongoose
          bucket.delete(mongoose.Types.ObjectId(file));
        });
        if (document) {
          await document.remove();
          return true;
        } else {
          return false;
        }
      },
    ),
    editDocument: combineResolvers(
      async (parent, { _id, title }, { models }) => {
        const document = await models.Document.findByIdAndUpdate(
          _id,
          { $set: { title } },
        );

        if (document) {
          return true;
        } else {
          return false;
        }
      },
    ),
  },

  // Document: {
  //   user: async (document, args, { loaders }) => {
  //     return await loaders.user.load(document.userId);
  //   },
  // },

  // Subscription: {
  //   DocumentCreated: {
  //     subscribe: () => pubsub.asyncIterator(EVENTS.DOCUMENT.CREATED),
  //   },
  // },
};
