import { GraphQLDateTime } from 'graphql-iso-date';

import userResolvers from './user';
import messageResolvers from './message';
import documentResolvers from './document';
import contactResolvers from './contact';
import departmentResolvers from './department';
import fileResolver from './file';
import tagResolver from './tag';

const customScalarResolver = {
  Date: GraphQLDateTime,
};

export default [
  customScalarResolver,
  userResolvers,
  messageResolvers,
  documentResolvers,
  contactResolvers,
  departmentResolvers,
  fileResolver,
  tagResolver
];
