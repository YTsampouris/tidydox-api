import { gql } from 'apollo-server-express';

export default gql`
  extend type Query {
    uploadedFiles: [File]
    file(_id: ID!): File
    downloadFile(_id: String): String
  }
  extend type Mutation {
    singleUploadLocal(doc_id: ID!, file: Upload!): File
    multipleUploadLocal(files: [Upload]!): [File]
    singleUploadS3(file: Upload!): File
    multipleUploadS3(files: [Upload]!): [File]
    deleteFile(_id: ID!): Boolean!
  }
  type File {
    _id: ID
    mimetype: String
    encoding: String
    filename: String
  }
`;
